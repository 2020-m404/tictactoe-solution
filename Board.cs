﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;

namespace TicTacToe
{
    public class Board
    {
        private const int ColCount = 3;
        private const int RowCount = 3;

        private Box[,] _boxes = new Box[ColCount, RowCount];

        private Player[] _players = new Player[2];

        private int _activePlayerIndex;

        public Player ActivePlayer { get => _players[_activePlayerIndex]; }

        public Player InactivePlayer { get => _players[1 - _activePlayerIndex]; }

        public Player BluePlayer
        {
            get => _players[0];
            set
            {
                _players[0] = value;
                _players[0].Color = "blue";
            }
        }

        public Player RedPlayer
        {
            get => _players[1];
            set
            {
                _players[1] = value;
                _players[1].Color = "red";
            }
        }

        public Box[,] Boxes { get => _boxes; }

        public Board()
        {
            for (int col = 0; col < ColCount; col++)
            {
                for (int row = 0; row < RowCount; row++)
                {
                    _boxes[col, row] = new Box(col, row);
                }
            }

            Random randomNumberGenerator = new Random();
            _activePlayerIndex = randomNumberGenerator.Next(0, 2);
        }

        public void ResetButtons()
        {
            foreach (Box box in _boxes)
            {
                box.Reset();
            }
        }

        private void ResetPlayers()
        {
            RedPlayer.Reset();
            BluePlayer.Reset();
        }

        public void PlayBox(int col, int row)
        {
            Box box = _boxes[col, row];
            box.SetOwner(ActivePlayer);

            // Add Winning Chance

            List<Box[]> mates = new List<Box[]>
            {
                GetHorizontalBoxMates(box),
                GetVerticalBoxMates(box)
            };

            mates.AddRange(GetDiagonalBoxMates(box));

            foreach (Box[] line in mates)
            {
                Box[] ownedMates = line.Where(box => box.Owner == ActivePlayer).ToArray();

                if (ownedMates.Length == 1)
                {
                    Box candidate = line.Except(ownedMates).First();
                    if (candidate.Owner == null)
                    {
                        ActivePlayer.AddWinningChance(candidate);
                    }
                }
            }

            // Remove winning chance from other player
            InactivePlayer.RemoveWinningChance(box);
        }

        public void TogglePlayer()
        {
            _activePlayerIndex = (_activePlayerIndex + 1) % 2;
        }

        public bool IsGameFinished(int col, int row)
        {
            return GetWinner(col, row) != null || !_boxes.Cast<Box>().ToList().Any(box => box.Owner == null);
        }

        public void AskForNewGame(Player winner = null)
        {
            string message = winner == null
                ? "It's a draw!"
                : $"{winner.Name.ToUpper()} ({winner.Color}) has won the game.";

            MessageBoxResult playAgain = MessageBox.Show(
                    $"{message}\n\nDo you want to play again?",
                    "We have a winner",
                    MessageBoxButton.YesNo
            );

            if (playAgain == MessageBoxResult.Yes)
            {
                ResetButtons();
                ResetPlayers();
            }
            else
            {
                Application.Current.Shutdown();
            }
        }

        public Player GetWinner(int col, int row)
        {
            Box box = _boxes[col, row];
            if (isWinnerCol(box) || isWinnerRow(box) || isWinnerDiagonal(box))
            {
                return _players[_activePlayerIndex];
            }
            return null;
        }

        private bool isWinnerCol(Box box)
        {
            Box[] mates = GetVerticalBoxMates(box);
            return mates[0].Owner == box.Owner && mates[1].Owner == box.Owner;
        }

        private bool isWinnerRow(Box box)
        {
            Box[] mates = GetHorizontalBoxMates(box);
            return mates[0].Owner == box.Owner && mates[1].Owner == box.Owner;
        }

        private bool isWinnerDiagonal(Box box)
        {
            List<Box[]> mates = GetDiagonalBoxMates(box);

            foreach (Box[] diagonaleMates in mates)
            {
                if (diagonaleMates[0].Owner == box.Owner && diagonaleMates[1].Owner == box.Owner)
                {
                    return true;
                }
            }

            return false;
        }

        private Box[] GetVerticalBoxMates(Box box)
        {
            int mateIndex = 0;
            Box[] mates = new Box[2];
            for (int i = 0; i < RowCount; i++)
            {
                Box candidate = _boxes[box.Col, i];
                if (candidate.Row != box.Row)
                {
                    mates[mateIndex++] = candidate;
                }
            }

            return mates;
        }

        private Box[] GetHorizontalBoxMates(Box box)
        {
            int mateIndex = 0;
            Box[] mates = new Box[2];
            for (int i = 0; i < ColCount; i++)
            {
                Box candidate = _boxes[i, box.Row];
                if (candidate.Col != box.Col)
                {
                    mates[mateIndex++] = candidate;
                }
            }

            return mates;
        }

        private List<Box[]> GetDiagonalBoxMates(Box box)
        {
            List<Box[]> mates = new List<Box[]>();

            if (box.Col == 1 && box.Row != 1 || box.Row == 1 && box.Col != 1)
            {
                return mates;
            }

            List<Box[]> diagonales = new List<Box[]>
            {
                new Box[] { _boxes[0, 0], _boxes[1, 1], _boxes[2, 2] },
                new Box[] { _boxes[0, 2], _boxes[1, 1], _boxes[2, 0] }
            };

            foreach (Box[] diagonale in diagonales)
            {
                if (diagonale.Contains(box))
                {
                    int mateIndex = 0;
                    Box[] diagonalMates = new Box[2];
                    foreach (Box candidate in diagonale)
                    {
                        if (candidate != box)
                        {
                            diagonalMates[mateIndex++] = candidate;
                        }
                    }
                    mates.Add(diagonalMates);
                }
            }

            return mates;
        }

    }
}
