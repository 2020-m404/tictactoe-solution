﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace TicTacToe
{
    public class Box
    {
        public int Col { get; private set; }
        public int Row { get; private set; }
        public Player Owner { get; private set; }
        public Button Button { get; set; }
        public Box(int col, int row)
        {
            Col = col;
            Row = row;
        }

        public void Reset()
        {
            Owner = null;
            Button.Content = new Image
            {
                Source = new BitmapImage(new Uri($@"Resources\Images\neutral.png", UriKind.Relative))
            };
            Button.IsEnabled = true;
            ResetBorder();
        }

        public void ResetBorder()
        {
            Button.BorderThickness = new Thickness(1);
            Button.BorderBrush = Brushes.Black;
        }

        public void SetOwner(Player player)
        {
            Owner = player;
            Button.Content = new Image
            {
                Source = new BitmapImage(new Uri($@"Resources\Images\{Owner.Color}.png", UriKind.Relative))
            };
            Button.IsEnabled = false;
        }
    }
}
