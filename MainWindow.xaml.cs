﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace TicTacToe
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private const int BoxDimension = 200;

        private Board _board;
        public MainWindow()
        {
            InitializeComponent();
            _board = new Board();
        }

        public void StartGame(object sender, RoutedEventArgs e)
        {
            string player1Name = Player1Name.Text;
            string player2Name = Player2Name.Text;

            if (!Player.IsValidName(player1Name))
            {
                DisplayPlayerNameError(1, player1Name);
                return;
            }
            else if (!Player.IsValidName(player2Name))
            {
                DisplayPlayerNameError(1, player1Name);
                return;
            }

            _board.BluePlayer = new Player(player1Name);
            _board.RedPlayer = new Player(player2Name);

            Player1Name.IsEnabled = false;
            Player2Name.IsEnabled = false;
            Player1Name.TextAlignment = TextAlignment.Center;
            Player2Name.TextAlignment = TextAlignment.Center;

            ActivatePlayer();

            InitializeField();
        }

        private void InitializeField()
        {
            foreach (Box box in _board.Boxes)
            {
                Button button = new Button
                {
                    Tag = $"{box.Col}_{box.Row}",
                    Width = BoxDimension,
                    Height = BoxDimension,
                };

                button.Click += OnBoxClick;

                Grid.SetColumn(button, box.Col);
                Grid.SetRow(button, box.Row);

                box.Button = button;

                TicTacToeBoard.Children.Add(button);
            }

            _board.ResetButtons();
        }

        private void DisplayPlayerNameError(int playerNumber, string playerName)
        {
            MessageBox.Show(
                $"Name of player {playerNumber} ({playerName}) is invalid.\nPlayers name must not be empty or longer than 20 characters.",
                "Invalid player name"
                );
        }

        private void OnBoxClick(object sender, RoutedEventArgs e)
        {
            string[] colAndRow = (sender as Button).Tag.ToString().Split('_');
            int col = int.Parse(colAndRow[0]);
            int row = int.Parse(colAndRow[1]);

            _board.PlayBox(col, row);

            if (_board.IsGameFinished(col, row))
            {
                Player winner = _board.GetWinner(col, row);
                if (winner != null)
                {
                    winner.WinGame();
                }
                else
                {
                    _board.RedPlayer.DrawGame();
                    _board.BluePlayer.DrawGame();
                }

                Score.Content = $"{_board.BluePlayer.Points}:{_board.RedPlayer.Points}";
                _board.AskForNewGame(winner);
            }

            _board.TogglePlayer();
            ActivatePlayer();

            ResetBoxBorders();
            HighlightWinningChances();
            HighlightLossPrevention();
        }

        private void ActivatePlayer()
        {
            var activePlayerTextbox = _board.ActivePlayer.Color == "blue" ? Player1Name : Player2Name;
            var inactivePlayerTextbox = _board.ActivePlayer.Color == "red" ? Player1Name : Player2Name;

            activePlayerTextbox.BorderBrush = new SolidColorBrush((Color)ColorConverter.ConvertFromString(_board.ActivePlayer.Color));
            activePlayerTextbox.BorderThickness = new Thickness(3);

            inactivePlayerTextbox.BorderBrush = Brushes.Black;
            inactivePlayerTextbox.BorderThickness = new Thickness(1);

            #region ALTERNATIVE

            /*
            
            Alternative method to do the same as above...

            Thickness activeBorder = new Thickness(3);
            Thickness inactiveBorder = new Thickness(1);

            if (player.Color == "blue")
            {
                Player1Name.BorderBrush = Brushes.Blue;
                Player1Name.BorderThickness = activeBorder;
                Player2Name.BorderBrush = Brushes.Black;
                Player2Name.BorderThickness = inactiveBorder;
            }
            else if (player.Color == "red")
            {
                Player1Name.BorderBrush = Brushes.Black;
                Player1Name.BorderThickness = inactiveBorder;
                Player2Name.BorderBrush = Brushes.Red;
                Player2Name.BorderThickness = activeBorder;
            }

            */

            #endregion ALTERNATIVE

        }

        private void ResetBoxBorders()
        {
            foreach (Box box in _board.Boxes)
            {
                box.ResetBorder();
            }
        }

        private void HighlightWinningChances()
        {
            Brush playerBrush = new SolidColorBrush((Color)ColorConverter.ConvertFromString(_board.ActivePlayer.Color));

            foreach (Box box in _board.ActivePlayer.WinningChances)
            {
                box.Button.BorderThickness = new Thickness(4);
                box.Button.BorderBrush = playerBrush;
            }
        }

        private void HighlightLossPrevention()
        {
            Brush playerBrush = new SolidColorBrush((Color)ColorConverter.ConvertFromString(_board.InactivePlayer.Color));

            foreach (Box box in _board.InactivePlayer.WinningChances)
            {
                box.Button.BorderThickness = new Thickness(2);
                box.Button.BorderBrush = playerBrush;
            }
        }
    }
}
