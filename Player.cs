﻿using System.Collections.Generic;

namespace TicTacToe
{
    public class Player
    {
        public string Name { get; }
        public string Color { get; set; }
        public int Points { get; private set; }
        public List<Box> WinningChances { get; } = new List<Box>();
        public Player(string name)
        {
            Name = name;
            Points = 0;
        }

        public static bool IsValidName(string name)
        {
            if (string.IsNullOrEmpty(name))
            {
                return false;
            }

            if (name.Length > 20)
            {
                return false;
            }

            return true;
        }

        public void WinGame()
        {
            Points += 3;
        }

        public void DrawGame()
        {
            Points += 1;
        }

        public void AddWinningChance(Box box)
        {
            WinningChances.Add(box);
        }

        public void RemoveWinningChance(Box box)
        {
            if (WinningChances.Contains(box))
            {
                WinningChances.Remove(box);
            }
        }

        public void Reset()
        {
            WinningChances.Clear();
        }
    }
}
